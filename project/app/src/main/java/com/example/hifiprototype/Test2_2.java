package com.example.hifiprototype;

import android.content.ClipData;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;


import androidx.appcompat.app.AppCompatActivity;

public class Test2_2 extends AppCompatActivity {
    private class DragShadow extends View.DragShadowBuilder {
        ColorDrawable greyBox;
        public DragShadow(View view) {
            super(view);
            greyBox = new ColorDrawable(Color.LTGRAY);
        }
        @Override
        public void onDrawShadow(Canvas canvas) {
            greyBox.draw(canvas);
        }
        @Override
        public void onProvideShadowMetrics(Point shadowSize, Point shadowTouchPoint) {
            View v = getView();
            int height = (int) v.getHeight();
            int width = (int) v.getWidth();
            greyBox.setBounds(0, 0, width, height);
            shadowSize.set(width, height);
            shadowTouchPoint.set((int)width/2, (int)height/2);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test2);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        View.OnLongClickListener listenClick = new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                ClipData data = ClipData.newPlainText("", "");
                DragShadow dragShadow = new DragShadow(v);
                v.startDrag(data, dragShadow, v, 0);
                return false;
            }
        };

        View.OnDragListener listenDrag = new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                int dragEvent = event.getAction();
                switch (dragEvent) {
                    case DragEvent.ACTION_DRAG_ENTERED:
                        break;
                    case DragEvent.ACTION_DRAG_EXITED:
                        break;
                    case DragEvent.ACTION_DROP:
                        ImageView target = (ImageView) v;
                        ImageView dragged = (ImageView) event.getLocalState();
                        Drawable target_draw = target.getDrawable();
                        Drawable dragged_draw = dragged.getDrawable();
                        dragged.setImageDrawable(target_draw);
                        target.setImageDrawable(dragged_draw);
                        break;
                }
                return true;
            }
        };

        findViewById(R.id.puppy_1).setOnLongClickListener(listenClick);
        findViewById(R.id.puppy_1).setOnDragListener(listenDrag);
        findViewById(R.id.puppy_2).setOnLongClickListener(listenClick);
        findViewById(R.id.puppy_2).setOnDragListener(listenDrag);
        findViewById(R.id.puppy_3).setOnDragListener(listenDrag);
        findViewById(R.id.puppy_3).setOnLongClickListener(listenClick);
        findViewById(R.id.puppy_4).setOnDragListener(listenDrag);
        findViewById(R.id.puppy_4).setOnLongClickListener(listenClick);

        View nextButton = findViewById(R.id.test2_next_button);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), Test3_1.class);
                startActivity(intent);
            }
        });
    }
}
