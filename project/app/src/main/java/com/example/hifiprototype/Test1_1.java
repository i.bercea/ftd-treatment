package com.example.hifiprototype;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;

public class Test1_1 extends AppCompatActivity {

    Test1_2 canvas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.test1intro);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        View startTest = findViewById(R.id.start_test_1);
        startTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setContentView(R.layout.test1);
                canvas = findViewById(R.id.mycanvas);

                final MediaPlayer sound = MediaPlayer.create(getBaseContext(), R.raw.test1_audio);
                ImageButton play_sound = findViewById(R.id.playsound);
                play_sound.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sound.start();
                    }
                });

                View nextButton = findViewById(R.id.test1_next_button);
                nextButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getBaseContext(), Test2_1.class);
                        startActivity(intent);
                    }
                });

            }
        });
    }

    public void clearCanvas(View view) {
        canvas.clearCanvas();
    }

}
