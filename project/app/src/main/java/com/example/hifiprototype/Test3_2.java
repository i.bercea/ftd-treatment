package com.example.hifiprototype;

import android.content.ClipData;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;


import androidx.appcompat.app.AppCompatActivity;

public class Test3_2 extends AppCompatActivity {
    private class DragShadow extends View.DragShadowBuilder {
        ColorDrawable greyBox;
        public DragShadow(View view) {
            super(view);
            greyBox = new ColorDrawable(Color.LTGRAY);
        }
        @Override
        public void onDrawShadow(Canvas canvas) {
            greyBox.draw(canvas);
        }
        @Override
        public void onProvideShadowMetrics(Point shadowSize, Point shadowTouchPoint) {
            View v = getView();
            int height = (int) v.getHeight();
            int width = (int) v.getWidth();
            greyBox.setBounds(0, 0, width, height);
            shadowSize.set(width, height);
            shadowTouchPoint.set((int)width/2, (int)height/2);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test3);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

//        final MediaPlayer sound = MediaPlayer.create(this, R.raw.test3_audio);
//        ImageButton play_sound = findViewById(R.id.playsound);
//        play_sound.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                sound.start();
//            }
//        });

        View.OnLongClickListener listenClick = new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                ClipData data = ClipData.newPlainText("", "");
                DragShadow dragShadow = new DragShadow(v);
                v.startDrag(data, dragShadow, v, 0);
                return false;
            }
        };

        View.OnDragListener listenDrag = new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                int dragEvent = event.getAction();
                switch (dragEvent) {
                    case DragEvent.ACTION_DRAG_ENTERED:
                    case DragEvent.ACTION_DRAG_EXITED:
                        break;
                    case DragEvent.ACTION_DROP:
                        ImageView target = (ImageView) v;
                        ImageView dragged = (ImageView) event.getLocalState();
//                        if((dragged.equals(findViewById(R.id.number_3)) && (target.equals(findViewById(R.id.number_6)) || target.equals(findViewById(R.id.number_9))))
//                        || (dragged.equals(findViewById(R.id.number_6)) && (target.equals(findViewById(R.id.number_3)) || target.equals(findViewById(R.id.empty_space))))
//                        || (dragged.equals(findViewById(R.id.number_9)) && (target.equals(findViewById(R.id.number_3)) || target.equals(findViewById(R.id.empty_space))))
//                        || (dragged.equals(findViewById(R.id.empty_space)) && (target.equals(findViewById(R.id.number_6)) || target.equals(findViewById(R.id.number_9)))))
                        Drawable target_draw = target.getDrawable();
                        Drawable dragged_draw = dragged.getDrawable();
                        dragged.setImageDrawable(target_draw);
                        target.setImageDrawable(dragged_draw);
                        break;
                }
                return true;
            }
        };

        findViewById(R.id.number_7).setOnLongClickListener(listenClick);
        findViewById(R.id.number_7).setOnDragListener(listenDrag);
        findViewById(R.id.number_8).setOnLongClickListener(listenClick);
        findViewById(R.id.number_8).setOnDragListener(listenDrag);
        findViewById(R.id.number_9).setOnDragListener(listenDrag);
        findViewById(R.id.number_9).setOnLongClickListener(listenClick);
        findViewById(R.id.empty_space).setOnDragListener(listenDrag);
        findViewById(R.id.empty_space).setOnLongClickListener(listenClick);

        View nextButton = findViewById(R.id.finish_test_3);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), Test4_1.class);
                startActivity(intent);
            }
        });
    }
}
